from app.modules.common.controller import Controller
from app.app import db
from app.utils.geo import geo_distance
from .passenger import Passenger
from datetime import date, time


class ControllerPassenger(Controller):
    def create(self, data):
        if not isinstance(data, dict):
            return False
        passenger = self._parse_passenger(data=data, passenger=None)
        db.session.add(passenger)
        db.session.commit()
        return passenger

    def get(self):
        passengers = Passenger.query.all()
        return passengers

    def get_by_id(self, object_id):
        passenger = Passenger.query.filter_by(passenger_id=object_id)
        return passenger

    def update(self, object_id, data):
        passenger = Passenger.query.filter_by(passenger_id=object_id)
        if passenger is None:
            return False
        passenger = self._parse_passenger(data=data, passenger=passenger)
        db.session.commit()
        return passenger

    def delete(self, object_id):
        passenger = Passenger.query.filter_by(passenger_id=object_id)
        db.session.delete(passenger)
        db.session.commit()

    def search(self, args):
        if args is None or not isinstance(args, dict):
            return None
        geo_long, geo_lat, country, city, street, max_distance, mode = None, None, None, None, None, None, None
        if 'geo_long' in args:
            geo_long = float(args['geo_long'])
        if 'geo_lat' in args:
            geo_lat = float(args['geo_lat'])
        if 'country' in args:
            country = args['country']
        if 'city' in args:
            city = args['city']
        if 'street' in args:
            street = args['street']
        if 'max_distance' in args:
            max_distance = float(args['max_distance'])
        if 'mode' in args:
            mode = args['mode']

        if geo_long is not None and geo_lat is not None and max_distance is not None:
            buyers = self._get_by_geo(geo_long=geo_long, geo_lat=geo_lat, max_distance=max_distance, mode=mode)
        else:
            buyers = self._get_by_address(country=country, city=city, street=street)
        return buyers

    # other functions
    def _get_by_geo(self, geo_lat, geo_long, max_distance, mode='km'):
        passengers = Passenger.query.all()
        ret_passengers = list()
        for passenger in passengers:
            distance = geo_distance((passenger.current_geo_long, passenger.current_geo_lat), (geo_long, geo_lat), mode=mode)
            if distance < float(max_distance):
                ret_passengers.append(passenger)
        return ret_passengers

        # passengers = Passenger.query.filter_by(
        #     geo_distance((Passenger.current_geo_long, Passenger.current_geo_lat), (geo_long, geo_lat),
        #                  mode=mode) <= max_distance).all()
        # return passengers

    def _get_by_address(self, country=None, city=None, street=None):
        query = db.session.query(Passenger)
        # users = None
        if country is not None:
            # users = User.query.filter_by(home_country=country).all()
            query = query.filter_by(home_country=country)
        if city is not None:
            # users = users.filter_by(home_city=city).all()
            query = query.filter_by(home_city=city)
        if street is not None:
            # users = users.filter_by(home_street=street).all()
            query = query.filter_by(home_street=street)
        users = query.all()
        return users

    def _parse_passenger(self, data, passenger=None):
        user_id, current_place, current_country, current_city, current_street, current_geo_long, current_geo_lat, from_place, from_country, from_city, from_street, from_geo_long, from_geo_lat, to_place, to_country, to_city, to_street, to_geo_long, to_geo_lat, go_date, go_time, number_people, chat_available, price_offer = None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None

        user_id = data['user_id']

        if 'current_place' in data:
            current_place = data['current_place']
        if 'current_country' in data:
            current_country = data['current_country']
        if 'current_city' in data:
            current_city = data['current_city']
        if 'current_street' in data:
            current_street = data['current_street']
        if 'current_geo_long' in data:
            current_geo_long = float(data['current_geo_long'])
        if 'current_geo_lat' in data:
            current_geo_lat = float(data['current_geo_lat'])

        if 'from_place' in data:
            from_place = data['from_place']
        if 'from_country' in data:
            from_country = data['from_country']
        if 'from_city' in data:
            from_city = data['from_city']
        if 'from_street' in data:
            from_street = data['from_street']
        if 'from_geo_long' in data:
            from_geo_long = float(data['from_geo_long'])
        if 'from_geo_lat' in data:
            from_geo_lat = float(data['from_geo_lat'])

        if 'to_place' in data:
            to_place = data['to_place']
        if 'to_country' in data:
            to_country = data['to_country']
        if 'to_city' in data:
            to_city = data['to_city']
        if 'to_street' in data:
            to_street = data['to_street']
        if 'to_geo_long' in data:
            to_geo_long = float(data['to_geo_long'])
        if 'to_geo_lat' in data:
            to_geo_lat = float(data['to_geo_lat'])

        if 'go_date' in data:
            if isinstance(data['go_date'], date):
                go_date = date.fromisoformat(data['go_date'])
        if 'go_time' in data:
            if isinstance(data['go_time'], time):
                go_time = time.fromisoformat(data['go_time'])

        if 'number_people' in data:
            number_people = int(data['number_people'])
        if 'chat_available' in data:
            chat_available = bool(data['chat_available'])
        if 'price_offer' in data['price_offer']:
            price_offer = float(data['price_offer'])

        if passenger is None:
            passenger = Passenger(user_id=user_id, current_place=current_place, current_country=current_country,
                                  current_city=current_city, current_street=current_street,
                                  current_geo_long=current_geo_long, current_geo_lat=current_geo_lat,
                                  from_place=from_place, from_country=from_country, from_city=from_city,
                                  from_street=from_street,
                                  from_geo_long=from_geo_long, from_geo_lat=from_geo_lat, to_place=to_place,
                                  to_country=to_country, to_city=to_city, to_street=to_street, to_geo_long=to_geo_long,
                                  to_geo_lat=to_geo_lat, go_date=go_date, go_time=go_time, number_people=number_people,
                                  chat_available=chat_available, price_offer=price_offer)
        else:
            passenger.user_id = user_id

            passenger.current_place = current_place
            passenger.current_country = current_country
            passenger.current_city = current_city
            passenger.current_street = current_street
            passenger.current_geo_long = current_geo_long
            passenger.current_geo_lat = current_geo_lat

            passenger.from_place = from_place
            passenger.from_country = from_country
            passenger.from_city = from_city
            passenger.from_street = from_street
            passenger.from_geo_long = from_geo_long
            passenger.from_geo_lat = from_geo_lat

            passenger.to_place = to_place
            passenger.to_country = to_country
            passenger.to_city = to_city
            passenger.to_street = to_street
            passenger.to_geo_long = to_geo_long
            passenger.to_geo_lat = to_geo_lat

            passenger.go_date = go_date
            passenger.go_time = go_time

            passenger.number_people = number_people
            passenger.chat_available = chat_available
            passenger.price_offer = price_offer
        return passenger
