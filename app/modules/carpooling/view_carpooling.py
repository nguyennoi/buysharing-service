from flask_restplus import Resource
from app.modules.common.decorator import token_required
from .dto_carpooling import DtoCarpooling
from .controller_carpooling import ControllerCarpooling

api = DtoCarpooling.api
carpooling = DtoCarpooling.model


@api.route('')
class CarpoolingList(Resource):
    # @token_required
    @api.marshal_list_with(carpooling)
    def get(self):
        controller = ControllerCarpooling()
        return controller.get()

    # @token_required
    @api.expect(carpooling)
    @api.marshal_with(carpooling)
    def post(self):
        data = api.payload
        controller = ControllerCarpooling()
        return controller.create(data=data)


@api.route('/<int:capooling_id>')
class Carpooling(Resource):
    # @token_required
    @api.marshal_with(carpooling)
    def get(self, carpooling_id):
        controller = ControllerCarpooling()
        return controller.get_by_id(object_id=carpooling_id)

    # @token_required
    @api.expect(carpooling)
    @api.marshal_with(carpooling)
    def put(self, carpooling_id):
        data = api.payload
        controller = ControllerCarpooling()
        return controller.update(object_id=carpooling_id, data=data)

    # @token_required
    def delete(self, carpooling_id):
        controller = ControllerCarpooling()
        return controller.delete(object_id=carpooling_id)
