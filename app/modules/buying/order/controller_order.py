from app.modules.common.controller import Controller
from .order import Order
from .dto_order import DtoOrder
from app.app import db
from datetime import date, time


class ControllerOrder(Controller):
    def create(self, data):
        if not isinstance(data, dict):
            return False
        order = self._parse_order(data=data, order=None)
        db.session.add(order)
        db.session.commit()
        return order

    def get(self):
        orders = Order.query.all()
        return orders

    def get_by_id(self, object_id):
        order = Order.query.filter_by(order_id=object_id).first()
        return order

    def update(self, object_id, data):
        if not isinstance(data, dict):
            return False
        order = Order.query.filter_by(order_id=object_id).first()
        if order is None:
            return False
        order = self._parse_order(data=data, order=order)
        db.session.commit()
        return order

    def delete(self, object_id):
        order = Order.query.filter_by(order_id=object_id).first()
        if order is None:
            return False
        db.session.delete(order)
        db.session.commit()
        return True

    def _parse_order(self, data, order=None):
        buyer_id, supplier_id, date_created, time_created, accepted, status, price, rate, comment, ship_price_buyer, ship_price_supplier, ship_price = None, None, None, None, None, None, None, None, None, None, None, None
        if 'buyer_id' in data:
            buyer_id = int(data['buyer_id'])
        if 'supplier_id' in data:
            supplier_id = int(data['supplier_id'])
        if 'date_created' in data:
            try:
                date_created = date.fromisoformat(data['date_created'])
            except Exception as e:
                print(e.__str__())
                pass
        if 'time_created' in data:
            try:
                time_created = time.fromisoformat(data['time_created'])
            except Exception as e:
                print(e.__str__())
                pass

        if 'accepted' in data:
            accepted = bool(data['accepted'])
        if 'status' in data:
            status = data['status']
        if 'price' in data:
            price = float(data['price'])
        if 'rate' in data:
            rate = int(data['rate'])

        if 'comment' in data:
            comment = data['comment']
        if 'ship_price_buyer' in data:
            ship_price_buyer = float(data['ship_price_buyer'])
        if 'ship_price_supplier' in data:
            ship_price_supplier = float(data['ship_price_supplier'])
        if 'ship_price' in data:
            ship_price = float(data['ship_price'])

        if order is None:
            order = Order(buyer_id=buyer_id, supplier_id=supplier_id, date_created=date_created,
                          time_created=time_created, accepted=accepted, status=status, price=price, rate=rate,
                          comment=comment, ship_price_buyer=ship_price_buyer, ship_price_supplier=ship_price_supplier,
                          ship_price=ship_price)
        else:
            order.buyer_id = buyer_id
            order.supplier_id = supplier_id
            order.date_created = date_created
            order.time_created = time_created

            order.accepted = accepted
            order.status = status
            order.price = price
            order.rate = rate

            order.comment = comment
            order.ship_price_buyer = ship_price_buyer
            order.ship_price_supplier = ship_price_supplier
            order.ship_price = ship_price
        return order
