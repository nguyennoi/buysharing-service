from flask_restplus import Resource
from .dto_order import DtoOrder
from .controller_order import ControllerOrder
from app.modules.common.decorator import token_required

api = DtoOrder.api
order = DtoOrder.model


@api.route('')
class OrderList(Resource):
    # @token_required
    @api.marshal_list_with(order)
    def get(self):
        """
        Get all orders in database.
        -----------------

        :return: List of orders.
        """
        controller = ControllerOrder()
        return controller.get()

    # @token_required
    @api.expect(order)
    @api.marshal_with(order)
    def post(self):
        """
        Create new order and save to database.
        -----------------
        :return: New order which has been created.
        """
        data = api.payload
        controller = ControllerOrder()
        return controller.create(data=data)


@api.route('/<int:order_id>')
class Order(Resource):
    # @token_required
    @api.marshal_with(order)
    def get(self, order_id):
        """
        Get all information about order by its ID.
        -------------------
        :param order_id: The ID of the order.

        :return: The order.
        """
        controller = ControllerOrder()
        return controller.get_by_id(object_id=order_id)

    # @token_required
    @api.expect(order)
    @api.marshal_with(order)
    def put(self, order_id):
        """
        Update existing order by its ID.
        ----------------

        :param order_id: The ID of the order.

        :return: The order after updating.
        """
        data = api.payload
        controller = ControllerOrder()
        return controller.update(object_id=order_id, data=data)

    # @token_required
    def delete(self, order_id):
        """
        Delete order in database by its ID.

        :param order_id: The ID of the order.

        :return: True if success and False vice versa.
        """
        controller = ControllerOrder()
        return controller.delete(object_id=order_id)
