from flask_restplus import Resource

from app.modules.common.decorator import token_required
from .controller_product import ControllerProduct
from .dto_product import ProductDto

api = ProductDto.api
product = ProductDto.model


@api.route('')
class ProductList(Resource):
    # @token_required
    @api.marshal_list_with(product)
    def get(self):
        """
        Return all products in table.

        :return: List of products
        """
        controller = ControllerProduct()
        return controller.get()

    @api.expect(product)
    @api.marshal_with(product)
    # @token_required
    def post(self):
        """
        Create new product to save to database and return it to client.

        :return: New product.
        """
        data = api.payload
        controller = ControllerProduct()
        return controller.create(data=data)


@api.route('/<int:product_id>')
@api.response(404, "Product not found")
@api.param('product_id', 'The product identifer')
class Product(Resource):
    @api.marshal_with(product)
    # @token_required
    def get(self, product_id):
        """
        Get information about product by its ID.

        :param product_id: The ID of product.

        :return: The product information.
        """
        controller = ControllerProduct()
        return controller.get_by_id(object_id=product_id)

    @api.expect(product)
    @api.marshal_with(product)
    # @token_required
    def put(self, product_id):
        """
        Update information of product.

        :param product_id: The ID of product.

        :return: The product after editing.
        """
        data = api.payload
        controller = ControllerProduct()
        return controller.update(object_id=product_id, data=data)

    # @token_required
    def delete(self, product_id):
        """
        Delete product in database.

        :param product_id: The ID of the product to delete.

        :return: True if success and False vice versa.
        """
        controller = ControllerProduct()
        return controller.delete(object_id=product_id)

@api.route('/search/<int:buyer_id>')
class BuyerProductList(Resource):
    @api.marshal_list_with(product)
    def get(self, buyer_id):
        controller = ControllerProduct()
        return controller.get_by_buyer_id(buyer_id = buyer_id)